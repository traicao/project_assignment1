
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
  tbody td>img {
    position: relative;
    max-width: 50px;
    float: left;
    margin-right: 15px;
}
     tbody td .user-link {
    display: block;
    font-size: 1.25em;
    padding-top: 3px;
    margin-left: 60px;
}
  </style>
</head>
<body>

<div class="container">
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
       <th >
                      <label  class="control-label" style="text-align:left">Member directory</label>
        </th>
      <th style="text-align:right">
                            <label  class="control-label">Search Members</label>
                           
                            <input id="txtSearch" type="text"  placeholder="Member name" value="<?php  echo $search;  ?>" />                              
                            
                            <button id="btnLogin" type="button"  class="btn btn-default btn-sm" style="text-align:right">Go</button>
                           
             
        </th>
      </tr>
    </thead>
    <tbody>
    <?php 
      foreach($rc as $r)
      {
        echo '   <tr>
                                    <td>
                                        <img src="Image/'.$r['image'].'" alt="">
                                        <a href="#" class="user-link">'.$r['firstname']. ' '.$r['surname']. ' </a>
                                        <span class="user-subhead">'.$r['job']. '</span>
                                    </td>
                                    <td style="text-align:right"><button id="btnLogin" type="button"  class="btn btn-info btn-sm" style="text-align:right">View</button></td>
                              
             </tr>';
      }
     ?>
                              
    </tbody>
  </table>
         <?php 
            if ($coutnPage>1)
            {
         ?>
                  <div  align="center">
                    <ul class = "pagination">
                     <li><a  <?php   if ($page>1 && isset($_GET['search_content'])) { echo 'href="index.php?page='.$pagePre.'&search_content='.$search.'"'; }
                                    if ($page>1 && !isset($_GET['search_content'])) { echo 'href="index.php?page='.$pagePre.'"'; }?>>Previous</a></li>
                     <?php
                      for($i=1; $i<=$coutnPage;$i++)
                      { 
                      ?>
                        <li <?php if ($i==$page) echo "class = 'active' "; ?> ><a  <?php  
                         if (isset($_GET['search_content'])) { echo 'href="index.php?page='.$i.'&search_content='.$search.'"'; }
                         else { echo 'href="index.php?page='.$i.'"'; }?>> <?php echo $i ?> </a></li>
                      <?php
                      }
                    ?>
                    <li><a <?php   if ($page<$coutnPage && isset($_GET['search_content'])) { echo 'href="index.php?page='.$pageNext.'&search_content='.$search.'"'; }
                                    if ($page<$coutnPage && !isset($_GET['search_content'])) { echo 'href="index.php?page='.$pageNext.'"'; }?>>Next</a></li>
                    </ul>
               </div>
          <?php 
            }
         ?>
</div>
<script>

$("#btnLogin").click(function(){
   var se=$("#txtSearch").val();
   window.location.href="./index.php?search_content="+se; 
});
</script>

</body>
</html>