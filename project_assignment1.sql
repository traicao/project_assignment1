-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 23, 2017 at 05:24 PM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project_assignment1`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `job` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `interests` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `specialism` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `password`, `firstname`, `surname`, `job`, `region`, `interests`, `specialism`, `image`) VALUES
(1, 'traicao@gmail.com', '12345abc', 'Trai', 'Cao', 'Developper', 'Quang Nam', 'Soccer', 'IT', 'traicao.png'),
(2, 'namle@gmail.com', 'abcde123', 'Nam', 'Le', 'Developer', 'Hue', 'Read IT News', 'IT', 'namle.png'),
(3, 'billgate@gmail.com', '123asdf', 'Bill', 'Gate', 'Manager', 'USA', 'Develop', 'Computer Science', 'billgate.png'),
(4, 'dongnhi@gmail.com', '12342536', 'Nhi', 'Dong', 'Singer', 'VietNam', 'shoppping', 'Sing', 'dongnhi.png'),
(5, 'hoailinh@gmail.com', '12536845', 'Linh', 'Hoai', 'Acter', 'VietNam', 'Movie', 'Perform', 'hoailinh.png'),
(6, 'hongocha@gmail.com', '1234567', 'Ha', 'Ho', 'Model', 'VietNam', 'Movie, Model, Sing', 'Model,Singer', 'hongocha.png'),
(7, 'khanhthi@gmail.com', '12345nbgf', 'Thi', 'Khanh', 'Dancer', 'VietNam', 'shopping,dance', 'Dancer', 'khanhthi.png'),
(8, 'nadal@gmail.com', '12345nbgf', 'Nadal', 'Rafael', 'Teniser', 'Spain', 'tennis,soccer', 'Sport', 'nadal.png'),
(9, 'phananh@gmail.com', '12345nbgf', 'Anh', 'Phan', 'MC', 'VietNam', 'sing,shopping', 'MC', 'phananh.png'),
(10, 'ronaldo@gmail.com', '12345nbgf', 'Ronaldo', 'Cristano', 'Footballer', 'Potugal', 'soccer,shopping', 'sport', 'ronaldo.png'),
(11, 'tranthanh@gmail.com', '12345nbgf', 'Thanh', 'Tran', 'Acter', 'VietNam', 'MC,Sing,Perform', 'Acter', 'tranthanh.png'),
(12, 'truonggiang@gmail.com', '12345nbgf', 'Giang', 'Truong', 'Acter', 'VietNam', 'MC,Perform', 'Acter', 'tranthanh.png'),
(13, 'hoquynhhuong@gmail.com', '12345nbgf', 'Huong', 'Ho', 'Singer', 'VietNam', 'Sing,Shopping', 'Singer', 'hoquynhhuong.png'),
(14, 'hoquynhhuong@gmail.com', '12345nbgf', 'Huong', 'Ho', 'Singer', 'VietNam', 'Sing,Shopping', 'Singer', 'hoquynhhuong.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
