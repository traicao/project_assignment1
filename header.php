<?php
error_reporting(E_ALL);
session_start();
define('ROOTPATH', __DIR__);
if(!isset($_COOKIE['email']))
{
    if(!isset($_SESSION['email']))
    {
        header('Location:login.php');
    }
}
else
{
    if(!isset($_SESSION['email']))
    {
        $_SESSION['email']=$_COOKIE['email'];
    }
}
?>