<?php 
session_start();
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Example of Bootstrap 3 Horizontal Form Layout</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.9.0/validator.min.js"></script>


<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
    .form-horizontal .control-label{
        padding-top: 7px;
    }
    
</style>
</head>
<body>
<div class="container" style="margin-top: 3%">
    <div class="row">
        <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class="">Existing members login</strong>
                
                </div>
                <div class="panel-body">
                    <div class="alert alert-danger" id="notice" style="display:none">
                     Username and/or Password incorrect. Try again!!!
                    </div>
                    <form   method="post"  class="form-horizontal"  data-toggle="validator">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputEmail13" placeholder="My email address"  
                                data-error="Bruh, that email address is invalid" required="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control"  data-minlength="6" 
                                 data-error="Minimum of 6 characters" id="inputPassword3" placeholder="Password" required="">
                                 <div class="help-block with-errors"></div>
                            </div>                           
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label class="">
                                        <input id="cbRemember" type="checkbox" class="">Remember me</label>
                                         <button id="btnLogin" type="button"  class="btn btn-default btn-sm" style="float:right">Login</button>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div> 		
<script type="text/javascript">
$('#btnLogin').click(function(){
    var em= $("#inputEmail13").val();
    var pass= $("#inputPassword3").val();
    var re=($('#cbRemember').is(':checked') )? 1 : 0;
    $.ajax({
        type: "POST",
        url: "./Controller/login.php",
        async:false,
        data:{
                email:em,
                password:pass,
                remember:re
        },
        success: function(data)
        {
            
            if(data =="success")
            {
               window.location.href="index.php";

            }
            if(data=="fail")
            {
               $('#notice').removeAttr("style");
                $('#notice').css('text-align','center')
            }
        },
        error:function()
        {
            alert("error");
        }

    });
});
</script>  
</body>
</html>                          
